# Docker Node Server for RosaeNLG

Docker image for RosaeNLG that contains essentially RosaeNLG's node server module `rosaenlg-node-server`.

For documentation, see [RosaeNLG documentation](https://rosaenlg.org).
